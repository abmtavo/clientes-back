﻿namespace clientes_back.Dto
{
    public class ClienteFiltroDTO
    {
        public string? Nombre { get; set; }

        public string? RFC { get; set; }

        public string? Estatus { get; set; }
    }
}
