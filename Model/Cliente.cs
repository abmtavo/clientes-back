﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace clientes_back.Model
{
    [Table(name:"Clientes")]
    public class Cliente
    {
        [Key]
        public int Id { get; set; }

        [Column(TypeName ="varchar(100)")]
        [Required]
        public string Nombre { get; set; }

        [Column(TypeName = "varchar(100)")]
        [Required]
        public string ApellidoPaterno { get; set; }

        [Column(TypeName = "varchar(100)")]
        [Required]
        public string ApellidoMaterno { get; set; }


        [Column(TypeName = "int")]
        [Required]
        public int Edad { get; set; }


        [Column(TypeName = "date")]
        [Required]
        public DateTime FechaNacimiento { get; set; }

        [Column(TypeName = "char(1)")]
        [Required]
        public string Genero { get; set; }


        [Column(TypeName = "varchar(10)")]
        [Required]
        public string EstadoCivil { get; set; }


        [Column(TypeName = "varchar(13)")]
        [Required]
        public string RFC { get; set; }

        [Column(TypeName = "varchar(200)")]
        [Required]
        public string Direccion { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required, EmailAddress]
        public string Email { get; set; }


        [Column(TypeName = "varchar(10)")]
        [Required, Phone]
        public string Telefono { get; set; }


        [Column(TypeName = "varchar(50)")]
        [Required]
        public string Puesto { get; set; }


        [Column(TypeName = "varchar(50)")]
        [Required]
        public string Estatus { get; set; }


        [Column(TypeName = "date")]
        [Required]
        public DateTime FechaAlta { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FechaBaja { get; set; }

    }
}
