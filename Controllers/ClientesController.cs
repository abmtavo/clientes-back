﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using clientes_back.Data;
using clientes_back.Model;
using clientes_back.Dto;

namespace clientes_back.Controllers
{
    [Route("api/clientes")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly ApplicationDBContext _context;

        public ClientesController(ApplicationDBContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cliente>>> GetListCliente()
        {
          if (_context.clientes == null)
          {
              return NotFound();
          }
            return await _context.clientes.Where(i=> i.Estatus== "Activo").ToListAsync();
        }

    
        [HttpGet("{id}")]
        public async Task<ActionResult<Cliente>> GetCliente(int id)
        {
            if (_context.clientes == null)
            {
                return NotFound();
            }
            var cliente = await _context.clientes.FindAsync(id);

            if (cliente == null)
            {
                return NotFound();
            }

            return cliente;
        }

        [HttpPut]
        public async Task<IActionResult> PutCliente(Cliente cliente)
        {
            if (cliente.Id== null)
            {
                return BadRequest();
            }

            _context.Entry(cliente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClienteExists(cliente.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<Cliente>> PostCliente(Cliente cliente)
        {
          if (_context.clientes == null)
          {
              return Problem("El cliente no debe de ser null.");
          }
            
            _context.clientes.Add(cliente);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCliente", new { id = cliente.Id }, cliente);
        }

        [HttpPost("getFilter")]
        public async Task<ActionResult<IEnumerable<Cliente>>> getFilter(ClienteFiltroDTO clienteFiltroDTO)
        {
            if (_context.clientes == null)
            {
                return NotFound();
            }
            var cliente = _context.clientes.Where(i=> i.Estatus == "Activo");
            if (!string.IsNullOrEmpty(clienteFiltroDTO.Nombre))
            {
                cliente= cliente.Where(i =>  EF.Functions.Like(i.Nombre, "%"+clienteFiltroDTO.Nombre+"%"));
            }
            if (!string.IsNullOrEmpty(clienteFiltroDTO.Estatus))
            {
                cliente = cliente.Where(i => EF.Functions.Like(i.Estatus , "%" + clienteFiltroDTO.Estatus + "%"));         
            }
            if (!string.IsNullOrEmpty(clienteFiltroDTO.RFC))
            {
                cliente = cliente.Where(i => EF.Functions.Like(i.RFC , "%" + clienteFiltroDTO.RFC + "%")); 
            }

            if (cliente == null)
            {
                return NotFound();
            }

            return await cliente.ToListAsync<Cliente>();
        }

        private bool ClienteExists(int id)
        {
            return (_context.clientes?.Any(e => e.Id == id)).GetValueOrDefault();
        }
    }
}
